#include <stdio.h>

#define FLATS_ON_FLOOR 4u

static void GetParameters(unsigned *_floorCount, unsigned *_flatNumber)
{
    int err = 0;
    unsigned floorCount = 0;
    unsigned porchCount = 0;
    unsigned flatNumber = 0;

    do {
        printf("Floor count: ");
        fflush(stdin); // Avoids scanf loop while argument is string
        err = scanf("%u", &floorCount);
    } while ((err != 1) || (floorCount < 1));

    do {
        printf("Porch count: ");
        fflush(stdin);
        err = scanf("%u", &porchCount);
    } while ((err != 1) || (porchCount < 1));

    do {
        printf("Flat number: ");
        fflush(stdin);
        err = scanf("%u", &flatNumber);

        if (flatNumber > (FLATS_ON_FLOOR * floorCount * porchCount)) {
            printf("Flat number is too large\n");
            flatNumber = 0; // "continue" can not be used here
        }
    } while ((err != 1) || (flatNumber < 1));

    if (_floorCount) {
        *_floorCount = floorCount;
    }

    if (_flatNumber) {
        *_flatNumber = flatNumber;
    }
}

static void GetFlatInfo(unsigned _floorCount, unsigned _flatNumber)
{
    static const char * const corners[FLATS_ON_FLOOR] = {
        "near left",
        "far left",
        "far right",
        "near right",
    };

    unsigned flatOffset = _flatNumber - 1;
    unsigned floorOffset = flatOffset / FLATS_ON_FLOOR;
    unsigned porch = 1u + floorOffset / _floorCount;
    unsigned floor = 1u + floorOffset % _floorCount;
    const char *corner = corners[flatOffset % FLATS_ON_FLOOR];

    printf("Floor: %u\nPorch: %u\nCorner: %s\n", floor, porch, corner);
}

int main(int argc, char *argv[])
{
    unsigned floorCount = 0;
    unsigned flatNumber = 0;

    (void) argc;
    (void) argv;

    GetParameters(&floorCount, &flatNumber);
    GetFlatInfo(floorCount, flatNumber);
    return 0;
}
